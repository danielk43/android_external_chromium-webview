Building the Chromium-based WebView in AOSP is no longer supported. WebView can
now be built entirely from the Chromium source code.

General instructions for building WebView from Chromium:
https://www.chromium.org/developers/how-tos/build-instructions-android-webview

As of version 97 the package name has changed to org.bromite.webview and must
be allowed in config_webview_packages.xml with optional signature. See:
https://github.com/bromite/bromite/wiki/Installing-SystemWebView

------

The prebuilt libwebviewchromium.so included in these APKs is built from Chromium
release tag 108.0.5359.156, using the GN build tool. To match Bromite build settings, set:

https://raw.githubusercontent.com/bromite/bromite/a0daaacf6dc55b692fdff58a982dd63e97ab7be6/build/bromite.gn_args

$$ depends on device ARCH
(00=arm, 50=arm64, 10=x86, 60=x64)

in your GN argument file before building.

------

For questions about building WebView, please see
https://groups.google.com/a/chromium.org/forum/#!forum/android-webview-dev
